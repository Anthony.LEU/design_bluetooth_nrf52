/*
 * Copyright (c) 2016 Open-RnD Sp. z o.o.
 * Copyright (c) 2020 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/sys/util.h>
#include <zephyr/sys/printk.h>
#include <inttypes.h>

/*
 * Get button configuration from the devicetree sw0 alias. This is mandatory.
 */
#define SW0_NODE	DT_ALIAS(sw0)
#define SW1_NODE	DT_ALIAS(sw1)
#define SW2_NODE	DT_ALIAS(sw2)
#define SW3_NODE	DT_ALIAS(sw3)
#if !DT_NODE_HAS_STATUS(SW0_NODE, okay)
#error "Unsupported board: sw0 devicetree alias is not defined"
#endif
#if !DT_NODE_HAS_STATUS(SW1_NODE, okay)
#error "Unsupported board: sw1 devicetree alias is not defined"
#endif
#if !DT_NODE_HAS_STATUS(SW2_NODE, okay)
#error "Unsupported board: sw2 devicetree alias is not defined"
#endif
#if !DT_NODE_HAS_STATUS(SW3_NODE, okay)
#error "Unsupported board: sw3 devicetree alias is not defined"
#endif
static const struct gpio_dt_spec button[4] = {GPIO_DT_SPEC_GET_OR(SW0_NODE, gpios,{0}),GPIO_DT_SPEC_GET_OR(SW1_NODE, gpios,{0}),GPIO_DT_SPEC_GET_OR(SW2_NODE, gpios,{0}),GPIO_DT_SPEC_GET_OR(SW3_NODE, gpios,{0})};
static struct gpio_callback button_cb_data;

void button_pressed(const struct device *dev, struct gpio_callback *cb,
		    uint32_t pins)
{
	printk("Button pressed at %" PRIu32 "\n", k_cycle_get_32());
}


int ledbutton_init(void)
{
	int ret;
    for (int x=0;x<4;x++){
        if (!gpio_is_ready_dt(&button[x])) {
            printk("Error: button device %s is not ready\n",
                button[x].port->name);
            return 0;
        }

        ret = gpio_pin_configure_dt(&button[x], GPIO_INPUT);
        if (ret != 0) {
            printk("Error %d: failed to configure %s pin %d\n",
                ret, button[x].port->name, button[x].pin);
            return 0;
        }

        ret = gpio_pin_interrupt_configure_dt(&button[x],
                            GPIO_INT_EDGE_TO_ACTIVE);
        if (ret != 0) {
            printk("Error %d: failed to configure interrupt on %s pin %d\n",
                ret, button[x].port->name, button[x].pin);
            return 0;
        }

        gpio_init_callback(&button_cb_data, button_pressed, BIT(button[x].pin));
        gpio_add_callback(button[x].port, &button_cb_data);
        printk("Set up button at %s pin %d\n", button[x].port->name, button[x].pin);
    }
    return 0;
}

int get_button(int x){
    return gpio_pin_get_dt(&button[x]);
}





//SYS_INIT(ledbutton_init, APPLICATION, CONFIG_APPLICATION_INIT_PRIORITY);