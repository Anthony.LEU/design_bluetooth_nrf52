/*
 * Copyright (c) 2018 Nordic Semiconductor ASA
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef ZEPHYR_INCLUDE_BLUETOOTH_SERVICES_DEMANDE_H_
#define ZEPHYR_INCLUDE_BLUETOOTH_SERVICES_DEMANDE_H_


#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define BT_UUID_DEMANDE 0x1500
#define BT_UUID_ECRIRE 0x1502


char* bt_get_ecrire(void);

int bt_set_ecrire(char* txt);

#ifdef __cplusplus
}
#endif



#endif /* ZEPHYR_INCLUDE_BLUETOOTH_SERVICES_DEMANDE_H_ */
