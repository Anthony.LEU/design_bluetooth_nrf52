/*
 * Copyright (c) 2018 Nordic Semiconductor ASA
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef ZEPHYR_INCLUDE_BLUETOOTH_SERVICES_LED_H_
#define ZEPHYR_INCLUDE_BLUETOOTH_SERVICES_LED_H_


#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define BT_UUID_LED 0x1400
#define BT_UUID_LED_ON 0x1401



bool bt_get_led_on(void);


int bt_set_led_toggle(void);


#ifdef __cplusplus
}
#endif



#endif /* ZEPHYR_INCLUDE_BLUETOOTH_SERVICES_LED_H_ */
