/** @file
 *  @brief GATT Battery Service
 */

/*
 * Copyright (c) 2018 Nordic Semiconductor ASA
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <errno.h>
#include <zephyr/init.h>
#include <zephyr/sys/__assert.h>
#include <stdbool.h>
#include <zephyr/types.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/uuid.h>
#include "demande.h"

#include <string.h>

#define LOG_LEVEL CONFIG_BT_LED_LOG_LEVEL
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(Demande);


char texte[32] ="";

static void blvl_ccc_cfg_changed(const struct bt_gatt_attr *attr,
				       uint16_t value)
{
	ARG_UNUSED(attr);

	bool notif_enabled = (value == BT_GATT_CCC_NOTIFY);

	LOG_INF("LED Notifications %s", notif_enabled ? "enabled" : "disabled");
}

static ssize_t read_blvl(struct bt_conn *conn,
			       const struct bt_gatt_attr *attr, void *buf,
			       uint16_t len, uint16_t offset)
{
	char lvl8[32];
	strcpy(lvl8,texte);

	return bt_gatt_attr_read(conn, attr, buf, len, offset, &lvl8,
				 sizeof(lvl8));
}

static ssize_t write_ecrire(struct bt_conn *conn,
			       const struct bt_gatt_attr *attr, void *buf,
			       uint16_t len, uint16_t offset, uint8_t flag)
{
	uint8_t* value = (uint8_t*) attr->user_data;
	memcpy(buf,value,len);
	bt_set_ecrire((char*) buf);
	return len;
}

BT_GATT_SERVICE_DEFINE(Demande,
	BT_GATT_PRIMARY_SERVICE(BT_UUID_DECLARE_16(BT_UUID_DEMANDE)),
	BT_GATT_CHARACTERISTIC(BT_UUID_DECLARE_16(BT_UUID_ECRIRE),
			       BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY | BT_GATT_CHRC_WRITE,
			       BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, read_blvl, write_ecrire,
			       &texte),
	BT_GATT_CCC(blvl_ccc_cfg_changed,
		    BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),
);

static int ecrire_init(void)
{
	strcpy(texte,"");
	return 0;
}

char* bt_get_ecrire(void)
{
	return texte;
}




int bt_set_ecrire(char* txt)
{
    int rc;

	strcpy(texte,txt);

	rc = bt_gatt_notify(NULL, &Demande.attrs[1], &texte, sizeof(texte));

	return rc == -ENOTCONN ? 0 : rc;
}


SYS_INIT(ecrire_init, APPLICATION, CONFIG_APPLICATION_INIT_PRIORITY);
