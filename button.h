
#ifndef ZEPHYR_INCLUDE_BLUETOOTH_SERVICES_BUTTON_H_
#define ZEPHYR_INCLUDE_BLUETOOTH_SERVICES_BUTTON_H_

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/sys/util.h>
#include <zephyr/sys/printk.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define BT_UUID_LED 0x1400
#define BT_UUID_LED_ON 0x1401

void button_pressed(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
int ledbutton_init(void);
int get_button();
void led_light(int val);


#ifdef __cplusplus
}
#endif



#endif /* ZEPHYR_INCLUDE_BLUETOOTH_SERVICES_BUTTON_H_ */
