# Design_Bluetooth_nRF52

## Description

Ce projet permet d'utiliser le nRF52 afin de communiquer en bluetooth pour réaliser certaines actions.
- les informations prévues par l'application bluetooth periferal hr
- (0x1401) un booléen qui change de valeur chaque seconde avec notification et lecture 
- (0x1501) une chaîne de caractère avec écriture, lecture et notification
- Les 4 BUTTON sont prévus pour pouvoir être utiliser mais ne sont pas tous associés
- La possibilité d'allumer la LED1 lorsque le BUTTON1 est enfoncé

## Démarrer

Avoir installé nRF connect avec une toolchain et toutes les extensions nrf sur Visual Studio Code.
Créer une nouvelle application a partir d'un "sample" nommé Bluetooth Peripheral HR 
Mettre Tous les fichiers .c et .h de ce projet dans le dossier src/ de l'application
Dans prj.conf modifier CONFIG_BT_DEVICE_NAME = "Le_nom_du_device"

## Utilisation

- Brancher la carte nRF52
- Build l'application
- Flash l'application
- Se connecter en bluetooth a la carte (avec par exemple nRF connect sur mobile)

## Limitations/Bugs

- Alerte LED dirigée par Demande Bouton directement sans être un service
- Absence de Alerte Buzzer
- Absence du Service System

## Annexe

- DesignTabService.xlsx (Le tableau de services et caractéristiques)

## Sources

L'application est basée sur l'application bluetooth periferal hr
